<?php
// Modals
?>
<div id="modalFons"></div>

<div class="modal">
    <span class="tanca">&times;</span>
        <h2><img class="colorCorall" src="imatges/nova01.svg" width="24" height="24"> CREA UNA NOVA PORRA</h2>
        
        <form id="novaPorra" action="<?php echo $arrel; ?>" method="post">

            <input type="text" name="novaPorra" class="inputes inputinv inputNom" maxlength="40" placeholder="Nom de la porra" required>
            
            <label class="labelElectes" for="novaElectes">
                Electes a escollir <input type="number" name="novaElectes" class="inputes inputinv" min="0" max="10000" placeholder="0" required>
            </label>
            <br>

            <input type="datetime-local" id="dataLimit" class="inputes inputinv" name="novaLimit">
            <label for="dataLimit">Data i hora límit</label>
            <br>

            <textarea class="inputes inputinv inputTextarea" name="novaDesc" maxlength="200" placeholder="Descripció"></textarea>
            <br>

            <table class="taulaes">
                <tr>
                    <th class="alineaEsquerra">Candidatures</th>
                    <th></th>
                </tr>
                <tr>
                    <td>
                        <input type="text" class="inputes inputinv candidatura" name="novaCand[]" maxlength="14" placeholder="Nom" required>
                    </td>
                    <td class='alineaDreta'>
                        <button type='button' class='enllaç creu candEliminar'>&#10006;</button>
                    </td>
                </tr>
                <tr id="ultimTr">
                    <td colspan="2">
                        <button type="button" id="candAfegir" class="enllaç">+ Afegir candidatura</button>
                    </td>
                </tr>
            </table>
            <br>
            <div class="flex">
                <div class="flex1">
                    <input type="hidden" name="novaOculta" value="">
                    <input type="submit" class="enviar" value="CREA">
                </div>
                <div class="flex4 textp">
                    *<strong>Electes</strong>: Posa "0" si vols que no hi hagi límit de repartir electes
                    <br>
                    *<strong>Data i hora límit</strong>: Deixa en blanc si no vols que hi hagi límit
                </div>
            </div>
        </form>
</div>


<div class="modal">
    <span class="tanca">&times;</span>
    <div class='flex'>
        <h2><img class="colorCorall" src="imatges/info01.svg" width="24" height="24"> Informació sobre elecPorra</h2>
    </div>
    <p>
        <strong><em>ElecPorra</em></strong> és un projecte d'aplicació web creat per divertimento per el gaudi de qui li pugui ser útil.
    </p>
    <p>
        Tota la informació que es publica en aquest web és oberta i potencialment hi pot accedir qualsevol persona.
        No introduïu informació ni dades personals o comprometedores.
    </p>
    <p>
        Tot el codi que s'utilitza ha estat desenvolupat expressament i es de codi obert.
        <br>
        HTML + CSS + PHP + Javascript
    </p>
    <p>
        Aplicació web creada per <a href='https://www.sacaix.com' target='_blank'>arnAu bellavista</a>.
    </p>
    <p>
        Podeu trobar el codi font del projecte a <a href='https://gitlab.com/sacaix/elecporra' target='_blank'>GitLab</a>.
    </p>
</div>
