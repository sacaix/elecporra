<?php
include('isset.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="cache-control" value="no-cache, no-store, must-revalidate">
		<meta http-equiv="Expires" content="Mon, 01 Jan 1990 00:00:01 GMT">
		
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<meta charset="UTF-8">
		<meta name="author" content="www.sacaix.com" />

		<link rel="icon" type="image/x-icon" href="favicon.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<title>elecPorra</title>
		<meta name='title' content="elecPorra" />
		<meta name='description' content="Porres electorals" />
		<meta name="language" content="ca" />

		<base href="<?php echo $protocol . $pag_host. $arrel; ?>" />

		<link rel="stylesheet" type="text/css" href="css/estils.css"/>
		<script type="text/javascript" src="js/script.js" defer></script>
	</head>
	
	<body>

		<?php include('modals.php'); ?>

		<header>
			<div class='flex'>
				<div class='flex1'>
					<a href='<?php echo $arrel; ?>'><img class='colorCorall' src='imatges/elecPorra00.svg'></a>
				</div>
				<div class='flex1 alineaDreta'>
					<button class="enllaç modalInfo"><img class="colorCorall" src="imatges/nova01.svg" width="18" height="18"> CREA UNA PORRA</button>
					<button class="enllaç modalInfo"><img class="colorCorall" src="imatges/info01.svg" width="18" height="18"> Info</button>
				</div>
			</div>
			<hr>
		</header>

		<main>

			<article>
			<?php
			// Mostrar la galeta sencera desencriptada
			// echo llegirGaleta('tot');

			// Consultar PORRA
			// A porra.php es comprovarà si l'arxiu existeix
			if(isset($_GET['p'])){
				$p = substr($_GET['p'],0,20); // Màxim 20 caracters
				$p = netejar($p);
				$p = str_replace(["'"," "], "", $p);
				if(!empty($p)){
					// Si s'ha introduït un nou resultat mostrem una pantalla intermitja
					if(!empty($nouResultat)){
						include('resultatNou.php');
					}else{
						include('porra.php');
					}
				}

						// Rebem una nova porra creada
			}else if(!empty($pn)){
				include('porraNova.php');

			// Rebem la porra eliminada
			}else if(!empty($eliminada)){
				include('porraEliminada.php');

			// Política de privadesa i cookies
			}else if(isset($_GET['privadesa'])){
				include('privadesa.php');

			// Gestió de cookies
			}else if(isset($_GET['cookies'])){
				include('cookies.php');

			// Zona ADMIN
			}else if(isset($_GET['control00'])){
				include('control00.php');

			// Error 404
			}else if(isset($_GET['error404'])){
				?>
				<h1>Error 404! Zona pròxima a la dimensió desconeguda</h1>
				<h3>Aquesta pàgina no existeix o ha sigut eliminada</h3>
				<?php

			}else{
				?>
				<div class='flex'>
					<div class='flex2'>
					<?php
					// Porres desades a la galeta
					$cookiesPorres = llegirGaleta('porres');
					if(!empty($cookiesPorres)){
						$cookiesPorres = explode(',',$cookiesPorres);
						?>
						<h2>Les meves porres</h2>
						<ul>
						<?php
						foreach($cookiesPorres as $v){
							if(!empty($v)){
								$arxiu = "docs/".$v.".json";
								if(file_exists($arxiu)){
									// Obrim la porra
									$arxiup = fopen($arxiu, "r") or die("Unable to open file!");
									$arxiuJSON = fread($arxiup,filesize($arxiu));
									fclose($arxiup);
									$dadesPorra = json_decode($arxiuJSON, true);
									echo "<li><a href='".$arrel.$v."'>".$dadesPorra['nom']."</a></li>";
								}
							}
						}
						?>
						</ul>
						<?php
					}
					// Resultats desats a la galeta
					$cookiesRes = llegirGaleta('resultats');
					if(!empty($cookiesRes)){
						$cookiesRes = explode('|',$cookiesRes);
						?>
						<h2>Porres a on he participat</h2>
						<ul>
						<?php
						foreach($cookiesRes as $v){
							$v = explode(',',$v);
							if(!empty($v[0])){
								$arxiu = "docs/".$v[0].".json";
								if(file_exists($arxiu)){
									// Obrim la porra
									$arxiup = fopen($arxiu, "r") or die("Unable to open file!");
									$arxiuJSON = fread($arxiup,filesize($arxiu));
									fclose($arxiup);
									$dadesPorra = json_decode($arxiuJSON, true);
									echo "<li><a href='".$arrel.$v[0]."'>".$dadesPorra['nom']."</a></li>";
								}
							}
						}
						?>
						</ul>
						<?php
					}
					?>
					</div>

					<div class='flex3 alineaDreta'>
						<?php include('cerca.php'); ?>
					</div>
				</div>

				<hr>

				<div class='flex'>
					<div class='flex1'>
						<p>
							<em><strong class='color'>elecPorra</strong></em> és una aplicació web que permet crear porres electorals
							per tal que hi pugui participar tothom que tingui l'enllaç o el codi.
						</p>
						<p>
							Els usuaris participants podran crear i eliminar els resultats que hagin publicat
							i l'usuari que hagi creat la porra podrà gestionar-la i eliminar-la gràcies a la informació desada a les galetes del navegador.
						</p>
						<p>
							<a class="enllaç" href="<?php echo $arrel; ?>privadesa">
								<img class="colorCorall" src="imatges/candau01.svg" width="16" height="16">
								Política de privadesa i cookies
							</a>
						</p>
					</div>

					<div class='flex1'>
						<p>
							<img class="colorCorall" src="imatges/dec01.svg">
						</p>
					</div>
				</div>
			<?php
			}
			?>
			</article>
		</main>

		<?php
		if(!isset($_COOKIE['cookies']) OR $_COOKIE['cookies'] != 'ok'){
		?>
		<!-- Avís de cookies -->
		<div class='cookies'>
			<input type="checkbox" id="cookiesButton"/>
			<input type="checkbox" id="cookiesButton2"/>
			<div class="cookiesAvis">
				<div class='flex'>
					<div class='flex3'>
						<img class='colorCorall' src="imatges/galeta01.svg" width="20" height="20">
						<strong>Informació important sobre cookies</strong>
						<br>
						Aquest web utilitza cookies pròpies per donar un millor servei,
						no s'utilitzen per recollir informació de caràcter personal.
						<br>
						Per a més informació consulti la nostra <strong><a href="<?php echo $arrel; ?>privadesa">política de privadesa i cookies</a></strong>
					</div>
					<div class='flex1'>
						<label id="cookiesOk" for="cookiesButton" class="enviar enviarCookies">ACCEPTAR</label>
						<label id="cookiesNo" for="cookiesButton2" class="enviar enviarCookies">REFUSAR</label>
					</div>
				</div>
			</div>
		</div>
		<?php
		}
		?>

	</body>
	
</html>