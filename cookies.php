<?php
// Pàgina de gestió de cookies
    // Porres desades a la galeta
    $cookiesHiHa = llegirGaleta('tot');
    // Porres desades a la galeta
    $cookiesPorres = llegirGaleta('porres');
    // Resultats desats a la galeta
    $cookiesRes = llegirGaleta('resultats');
    // Llegir galetes cookies
    $cookiesCookies = llegirGaletaCookies();
?>
<h1>Galetes / Cookies</h1>

<div class='flex'>
    <div class='flex1'>
        <h2>Les meves porres</h2>
        <?php
        if(!empty($cookiesPorres)){
            $cookiesPorres = explode(',',$cookiesPorres);
            ?>
            <table class="taulaes">
            <?php
            foreach($cookiesPorres as $v){
                if(!empty($v)){
                    $arxiu = "docs/".$v.".json";
                    if(file_exists($arxiu)){
                        // Obrim la porra
                        $arxiup = fopen($arxiu, "r") or die("Unable to open file!");
                        $arxiuJSON = fread($arxiup,filesize($arxiu));
                        fclose($arxiup);
                        $dadesPorra = json_decode($arxiuJSON, true);
                        $porraEnllaç = "<a href='".$arrel.$v."'>".$dadesPorra['nom']."</a>";
                    }else{
                        $porraEnllaç = "<em>Porra eliminada</em>";
                    }
                    ?>
                    <tr>
                        <td>
                            <?php echo $porraEnllaç; ?>
                        </td>
                        <td class="alineaDreta">
                            <form action="<?php echo $arrel; ?>cookies" method="post">
                                <input type="hidden" name="eliminarPorraGaleta" value="<?php echo $v; ?>">
                                <button type="submit" class="enllaç creu resEliminar"
                                    onclick="return confirm('Estàs segur que vols eliminar la galeta que et defineix com a creador d\'aquestas porra?');">&#10006;</button>
                            </form>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
                <tr><td colspan="2"></td></tr>
            </table>
            <?php
        }else{
            ?>
            <p class="color">No has creat cap porra</p>
            <?php
        }
        ?>
    </div>

    <div class='flex1'>
    <h2>Porres a on he participat</h2>
        <?php
        // Resultats desats a la galeta
        $cookiesRes = llegirGaleta('resultats');
        if(!empty($cookiesRes)){
            $cookiesRes = explode('|',$cookiesRes);
            ?>
            <table class="taulaes">
            <?php
            foreach($cookiesRes as $v){
                $v = explode(',',$v);
                if(!empty($v[0])){
                    $arxiu = "docs/".$v[0].".json";
                    if(file_exists($arxiu)){
                        // Obrim la porra
                        $arxiup = fopen($arxiu, "r") or die("Unable to open file!");
                        $arxiuJSON = fread($arxiup,filesize($arxiu));
                        fclose($arxiup);
                        $dadesPorra = json_decode($arxiuJSON, true);
                        $porraEnllaç = "<a href='".$arrel.$v[0]."'>".$dadesPorra['nom']."</a>";
                    }else{
                        $porraEnllaç = "<em>Porra eliminada</em>";
                    }
                    ?>
                    <tr>
                        <td>
                            <?php echo $porraEnllaç; ?>
                        </td>
                        <td class="alineaDreta">
                            <form action="<?php echo $arrel; ?>cookies" method="post">
                                <input type="hidden" name="eliminarParticipacio" value="<?php echo $v[0]; ?>">
                                <button type="submit" class="enllaç creu resEliminar"
                                    onclick="return confirm('Estàs segur que vols eliminar la participació a aquesta porra?');">&#10006;</button>
                            </form>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
                <tr><td colspan="2"></td></tr>
            </table>
            <?php
        }else{
            ?>
            <p class="color">No has participat a cap porra</p>
            <?php
        }
        ?>
    </div>
</div>


<h2>Eliminar les galetes</h2>
<div class='flex'>
    <?php
    if(empty($cookiesPorres) && empty($cookiesCookies)){
            ?>
            <p class="color">No tens cap galeta activa desada al navegador</p>
            <?php
    }else{
        // Eliminar cookies de porres i resultats
        if(!empty($cookiesHiHa)){
            ?>
            <div class='flex1'>
                <form action="<?php echo $arrel; ?>cookies" method="post">
                    <input type="hidden" name="eliminarCookies" value="">
                    <button type="submit" class="enviar"
                        onclick="return confirm('Estàs segur que vols eliminar aquestes galetes/cookies?');">
                        &#10006; Eliminar la cookies de totes les porres creades i particpades
                    </button>
                </form>
            </div>
            <?php
        }
        // Eliminar cookie de cookies
        if(!empty($cookiesCookies)){
            ?>
            <div class='flex1'>
                <button id="cookiesNoButton" type="submit" class="enviar">
                    &#10006; Eliminar la cookie per saber si s'han acceptat les cookies
                </button>
            </div>
            <?php
        }
    }
    ?>
</div>