<?php
// Nova porra / Pantalla intermitja
// PHP i JSON
// https://www.w3schools.com/Php/php_json.asp
?>
<div class='flex'>
    <div class='flex1'>
        <h1><?php echo $novaNom; ?></h1>
        <p><?php echo $novaDesc; ?></p>
        <?php
        if(!empty($novaElectes)){
            ?><p><?php echo $novaElectes; ?> electes</p><?php
        }
        ?>
    </div>
</div>

<hr>

<div class=flex>
    <div class='flex1'>
        <p>Comparteix aquest codi amb tothom qui vulguis que ompli la porra:</p>
        <h2 class='color'><?php echo $pn; ?></h2>
        <p>
            O comparteix directament l'enllaç de la porra:
            <br>
            <h3 class='color'><?php echo $urlcompleta; ?><?php echo $pn; ?></h3>
        </p>
        <p class='alineaCentre'>
            <form method="post" action="<?php echo $arrel.$pn; ?>">
                <button type="submit" class="enviar">ACCEDEIX A LA PORRA</button>
            </form>
        </p>
        <p>
            Tothom que tingui l'enllaç o el codi, podrà accedir i participar lliurement.
            <br>
            Tota la informació que es publica en aquest web és oberta i potencialment hi pot accedir qualsevol persona.
            No poseu informació ni dades personals o comprometedores.
        </p>
    </div>
</div>