<?php
// PHP i JSON
// https://www.w3schools.com/Php/php_json.asp


if(file_exists("docs/".$p.".json")){

    // Obrim la porra
    $arxiup = fopen("docs/".$p.".json", "r") or die("Unable to open file!");
    $arxiuJSON = fread($arxiup,filesize("docs/".$p.".json"));
    fclose($arxiup);
    $dadesPorra = json_decode($arxiuJSON, true);

    // Agafem els codis dels resultats omplerts d'aquesta porra
    $codisResArray = galetaResultatsPorra($p);

    // Mirem si la porra ha passat la data límit de tancament
    $oberta = porraActiva($dataActual, $dadesPorra['limit']);

    // Mirem si la porra pertany a l'usuari
    $porraMeva = porraMeva($p);

    // Convertim les candidatrues a un array
    $dadesCand = explode(',', $dadesPorra['candidatures']);
    ?>
    <div class='flex'>
        <div class='flex1'>
            <h1><?php echo $dadesPorra['nom']; ?></h1>
            <p><?php echo $dadesPorra['descripcio']; ?></p>
            <?php
            if(!empty($dadesPorra['electes'])){
                ?><p><?php echo $dadesPorra['electes']; ?> electes</p><?php
            }
            ?>
        </div>
        <div class='flex1 alineaDreta'>
            <?php
            if($porraMeva && !$oberta){
                ?>
                <button type="button" id="porraAfegir" class="enviar modalInfo"><strong>&#43;</strong> Resultat final</button>
                <?php
            }else{
                ?>
                 <button type="button" id="porraAfegir" class="enviar modalInfo" <?php if(!$oberta){ echo "disabled"; }?>><strong>&#43;</strong> Afegir resultats</button>
                <?php
            }
            if($porraMeva){
                ?>
                <button type="button" id="porraEditar" class="enviar modalInfo">Editar</button>
                <?php
            }
            ?>
            <button type="button" id="porraConf" class="enviar modalInfo"><img class="colorBlanc" src="imatges/comp01.svg" width="20" height="20"></button>
        </div>
    </div>

    <div class='flex1'>
        <?php
        // Si la porra està oberta deixem entrar nous resultats
        if($oberta){
            $diaMesAny = date('d-m-Y', strtotime($dadesPorra['limit']));
            $hora = date('H:i', strtotime($dadesPorra['limit']));
            ?>
            <p>Porra oberta fins el <?php echo $diaMesAny; ?> a les <?php echo $hora; ?>h</p>
            <?php
        }else{
            ?>
            <div class="quadre colorFons">
                <strong><em>PORRA TANCADA</em></strong>
                <br>
                Aquest porra està fora de límit i no es poden introduïr nous resultats.
            </div>
            <?php
        }
        ?>
    </div>


    
        <table class="taulaes">
            <tr>
                <th></th>
                <?php
                $numc = 0;
                foreach($dadesCand as $pc){
                    $numc++;
                    ?>
                    <th class='alineaCentre'><?php echo $pc; ?></th>
                    <?php
                }
                ?>
                <th>
                    <?php
                    if(!empty($dadesPorra['final'])){
                        echo "Dif.";
                    }
                    ?>
                </th>
            </tr>
            <?php
            if(!empty($dadesPorra['final'])){
                ?>
                <tr class="resultatFinal">
                    <td class='alineaEsquerra'><strong class='margeEsq'>Resultat Final</strong></td>
                    <?php
                    // Recorrem l'array de resultats final
                    $finalex = explode(',', $dadesPorra['final']);
                    foreach($finalex as $porraRes){
                        ?><td class='resultatNum alineaCentre'><?php echo $porraRes; ?></td>
                    <?php
                    }
                    ?>
                    <td></td>
                </tr>
                <?php
            }
            



            if(!empty($dadesPorra['porres'])){
                // Recorrem l'array de resultats
                $podi = 0;
                $difAnterior = 0;
                foreach($dadesPorra['porres'] as $porraRes){
                    if(in_array($porraRes['clau'], $codisResArray)){
                        $color = "color";
                    }else{
                        $color= '';
                    }
                    ?>
                    <tr>
                        <td>
                            <span class='<?php echo $color; ?>'>
                                <?php
                                if(!empty($dadesPorra['final']) && (!empty($porraRes['diferencia']) OR $porraRes['diferencia'] == 0)){
                                    if($porraRes['diferencia'] == 0){
                                        $podi = 1;
                                    }else if($porraRes['diferencia'] != $difAnterior){
                                        $podi++;
                                    }
                                    if($podi == 1){
                                        echo "<img class='IconaMig' src='imatges/estrella01.svg' width='22' height='22'> ";
                                    }else if($podi == 2){
                                        echo "<img class='IconaMig' src='imatges/estrella02.svg' width='22' height='22'> ";
                                    }else if($podi == 3){
                                        echo "<img class='IconaMig' src='imatges/estrella03.svg' width='22' height='22'> ";
                                    }
                                    $difAnterior = $porraRes['diferencia'];
                                }
                                ?>
                                <?php echo $porraRes['nom']; ?>
                            </span>
                        </td>
                    <?php
                    $porraA = explode(',', $porraRes['porra']);
                    foreach($porraA as $res){
                        ?>
                        <td class='resultatNum alineaCentre <?php echo $color; ?>'><?php echo $res; ?></td>
                        <?php
                    }
                    ?>
                    <td class='alineaCentre'>
                        <?php
                        if($oberta && in_array($porraRes['clau'], $codisResArray)){
                            ?>
                            <form action="<?php echo $arrel.$p; ?>" method="post">
                                <input type="hidden" name="codiPorra" value="<?php echo $p; ?>">
                                <input type="hidden" name="eliminarRes" value="<?php echo $porraRes['clau']; ?>">
                                <button type="submit" class="enllaç creu resEliminar" onclick="return confirm('Estàs segur que vols eliminar aquesta resultat?');">&#10006;</button>
                            </form>
                            <?php
                        }else if(!empty($dadesPorra['final'])){
                            if(!empty($porraRes['diferencia']) OR $porraRes['diferencia'] == 0){
                                if($porraRes['diferencia'] > 0){ echo "+"; }
                                echo "<strong>".$porraRes['diferencia']."</strong>";
                            }
                        }
                        ?>
                    </td>
                </tr>
                <?php
                }
            }else{
                ?>
                <td colspan="<?php echo $numc+2; ?>">
                    <span class="margeEsq">Ningú ha participat, encara no hi ha cap resultat</span>
                </td>
                <?php
            }
            ?>
        </tr>
            <tr>
                <td></td>
                <td colspan="<?php echo $numc+1; ?>"></td>
            </tr>
        </table>




    <!-- MODALS -->
    <!-- Modal afegir resultats -->
    <div class="modal">
        <span class="tanca">&times;</span>
        <h1>Afegir resultat<?php if($porraMeva && !$oberta){ echo " final"; }?></h1>
        <?php
        if(!empty($dadesPorra['electes'])){
            $max = $dadesPorra['electes'];
            $disabled = "disabled";
            ?>
            <h3>
                <?php echo $dadesPorra['electes']; ?> electes | Falta repartir:
                <span id="electesFalten" num="<?php echo $dadesPorra['electes']; ?>">
                    <?php echo $dadesPorra['electes']; ?>
                </span>
            </h3>
            <?php
        }else{
            $max = 999999999;
            $disabled = "";
            ?><span id="electesFalten" num="0"></span><?php
        }
        ?>

        <hr>

        <form id="nouResultat" action="<?php echo $arrel.$p; ?>" method="post">
            <?php
            if($porraMeva && !$oberta){
                ?><input type='hidden' id='porrista' name='porrista' maxlength='28' value='Resultat final'><?php
            }else{
                ?><input type='text' id='porrista' name='porrista' class='inputes inputinv' maxlength='28' placeholder='El teu nom' required><?php
            }
            ?>
            <table class="taulaes">
                <tr>
                    <th class='alineaEsquerra'>Candidatura</th>
                    <th class='alineaEsquerra'>Electes</th>
                </tr>
                <?php
                    foreach($dadesCand as $pc){
                        ?>
                        <tr>
                            <td>
                                <?php echo $pc; ?>
                            </td>
                            <td class="alineaDreta">
                                <input type="number" class="resultat inputes inputinv alineaDreta" name="electes[]" min="0" max="<?php echo $max; ?>" placeholder="0">
                            </td>
                        </tr>
                        <?php
                    }
                ?>
                <tr>
                    <td colspan='2'></th>
                </tr>
            </table>
            <input type="hidden" name="porraCodi" value="<?php echo $p; ?>">
            <input type="hidden" name="porraNom" value="<?php echo $dadesPorra['nom']; ?>">
            <input type="hidden" name="nouOcult" value="">
            <?php
            // Si la porra està oberta deixem entrar nous resultats
            if($oberta){
                ?>
                <div class="flex">
                    <p class="flex1"><input type="submit" id="afegirSumbit" class="enviar" value="ENVIA" <?php echo $disabled; ?>></p>
                    <div class="flex4 textp">
                        Tota la informació que es publica en aquest web és oberta i potencialment hi pot accedir qualsevol persona.
                        No introduïu informació ni dades personals o comprometedores.
                    </div>
                </div>
                <?php
            }else{
                // Si és el creador de la porra pot afegir els resultats final
                if($porraMeva){
                    ?>
                    <div class="flex">
                        <p class="flex1">
                            <input type="hidden" name="final">
                            <input type="submit" id="afegirSumbit" class="enviar" value="ENVIA" <?php echo $disabled; ?>>
                        </p>
                        <div class="flex4 textp">
                            Afegir el resultat final permet calcular automàticament qui s'ha acostat més i crear un rànquing.
                        </div>
                    </div>
                    <?php
                }else{
                    ?>
                    <p>Aquest porra està fora de límit i no es poden introduïr nous resultats.</p>
                    <br>
                    <button class="enviar" disabled>PORRA TANCADA</button>
                    <?php
                }
            }
            ?>
        </form>
    </div>



    <!-- Modal EDITAR PORRA -->
    <?php
    if($porraMeva){
    ?>
        <div class="modal">
            <span class="tanca">&times;</span>
            <h1>Editar la porra</h1>


            <form id="editaPorra" action="<?php echo $arrel.$p; ?>" method="post">

                <input type="text" name="editaPorra" class="inputes inputinv inputNom" maxlength="40" value="<?php echo $dadesPorra['nom']; ?>" placeholder="Nom de la porra" required>

                <label class="labelElectes" for="editaElectes">
                    Electes a escollir <input type="number" name="editaElectes" class="inputes inputinv" min="0" max="10000" value="<?php echo $dadesPorra['electes']; ?>" placeholder="0" required>
                </label>
                <br>

                <input type="datetime-local" id="dataLimit" class="inputes inputinv" value="<?php echo $dadesPorra['limit']; ?>" name="editaLimit">
                <label for="dataLimit">Data i hora límit</label>
                <br>

                <textarea class="inputes inputinv inputTextarea" name="editaDesc" maxlength="200" placeholder="Descripció"><?php echo $dadesPorra['descripcio']; ?></textarea>
                <br>

                <table class="taulaes">
                    <tr>
                        <th class="alineaEsquerra">Candidatures</th>
                    </tr>
                    <?php
                    // Recorrem l'array de resultats
                    foreach($dadesCand as $pCand){
                    ?>
                        <tr>
                            <td>
                                <input type="text" class="inputes inputinv candidatura" name="editaCand[]" maxlength="14" value="<?php echo $pCand; ?>" placeholder="Nom" required>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    <tr><td></td></tr>
                </table>

                <p class="textp">
                    *<strong>Electes</strong>: Posa "0" si vols que no hi hagi límit de repartir electes
                    <br>
                    *<strong>Data i hora límit</strong>: Deixa en blanc si no vols que hi hagi límit
                </p>
                <input type="hidden" name="codiPorra" value="<?php echo $p; ?>">
                <input type="hidden" name="editaOculta" value="">
                <input type="submit" class="enviar" value="EDITA">
            </form>
            
            <form class="alineaDreta" method="post" action="<?php echo $arrel; ?>" onsubmit="return confirm('Estàs segur que vols eliminar aquesta porra?');">
                <input type="hidden" name="eliminarOculta" value="">
                <input type="hidden" name="eliminarNom" value="<?php echo $dadesPorra['nom']; ?>">
                <input type="hidden" name="eliminar" value="<?php echo $p; ?>">
                <button type="submit" class="enviar alineaDreta"><img class="colorBlanc" src="imatges/brossa01.svg" width="20" height="20"> Elimina aquesta porra</button>
            </form>
        </div>
    <?php
    }
    ?>


    <!-- Modal info PORRA -->
    <div class="modal">
        <span class="tanca">&times;</span>
        <h2>Comparteix la porra <img class="colorBlanc" src="imatges/comp01.svg" width="24" height="24"></h2>

        <p>
            <a href="http://twitter.com/share?text=Et convido a participar a la meva porra: <?php echo $dadesPorra['nom']; ?>&url=<?php echo $urlcompleta; ?>&hashtags=elecPorra" target="_blank">
                <img src="imatges/twitter01.svg" width="60" height="60" />
            </a>
            &nbsp;
            <a href="https://telegram.me/share/url?url=<?php echo $urlcompleta; ?>&text=Et convido a participar a la meva porra: <?php echo $dadesPorra['nom']; ?>" target="_blank">
                <img src="imatges/telegram01.svg" width="60" height="60" />
            </a>
            &nbsp;
            <a href="https://api.whatsapp.com/send?text=<?php echo $urlcompleta; ?> Et convido a participar a la meva porra: <?php echo $dadesPorra['nom']; ?>" target="_blank" data-action="share/whatsapp/share">
                <img src="imatges/whatsapp01.svg" width="60" height="60" />
            </a>
            &nbsp;
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $urlcompleta; ?>" target="_blank">
                <img src="imatges/facebook01.svg" width="60" height="60" />
            </a>
        </p>

        <p>Copia i introdueix el codi de la porra o bé comparteix-la directament a través del seu enllaç:</p>
        <h3 class='color'><?php echo $p; ?></h3>
        <h3 class='color'><?php echo $urlcompleta; ?></h3>
        <p class='textp'>
            Tothom que tingui l'enllaç o el codi, podrà accedir i participar lliurement.
            <br>
            Tota la informació que es publica en aquest web és oberta i potencialment hi pot accedir qualsevol persona.
            No introduïu informació ni dades personals o comprometedores.
        </p>
    </div>
<?php


}else{
    // Si no hi ha l'arxiu avisem
    ?>
    <div class='alineaDreta'>
        <?php include('cerca.php'); ?>
    </div>
    <h1>Error! Porra no trobada</h1>
    <h3>Aquesta porra no existeix o ha sigut eliminada</h3>
    <?php
}