// arnAu bellavista 2023
// elecPorra
// Script JS

// Prevé reenviar formularis quan es refresca la pàgina
if(window.history.replaceState){
	window.history.replaceState(null, null, window.location.href);
}

// CREAR PORRA
document.getElementById('candAfegir').onclick = function(){
	// Afegir una fila a la taula candidatures
	var candTr = document.createElement("tr");
		candTr.setAttribute('class', 'pista');
		var candTd1 = document.createElement("td");
			candTd1.innerHTML = "<input type='text' class='inputes inputinv candidatura' name='novaCand[]' maxlength='14' placeholder='Nom'>";
			candTr.appendChild(candTd1);
		var candTd2 = document.createElement("td");
			candTd2.setAttribute('class', 'alineaDreta');
			candTd2.innerHTML = "<button type='button' class='enllaç creu candEliminar'>&#10006;</button>";
			candTr.appendChild(candTd2);
		document.getElementById('ultimTr').before(candTr);
	// Activem els buttons de les candidatures
	activarCandidatures();
}

// Eliminar candidatura
function activarCandidatures(){
	var candEliminar = document.getElementsByClassName("candEliminar");
	for(let i=0; i<candEliminar.length; i++){
		candEliminar[i].onclick = function(){
			eliminarFila(candEliminar[i].parentElement.parentElement);
		}
	}
}

function eliminarFila(e){
	// Esborrem la fila de la taula
	e.remove();
	// Reactivem els buttons de les candidatures
	activarCandidatures();
}
// fi Crear Porra



// AFEGIR RESULTATS
let porraAfegir = document.getElementById('porraAfegir');
if(porraAfegir){
	// Agafem el nombre d'electes per respartir
	var electesFalten = document.getElementById('electesFalten');
	var nume = electesFalten.getAttribute("num");

	if(nume != 0){
		// Càlcul d'electes que falta repartir
		document.getElementById('nouResultat').onchange = function(){
			var resultat = document.getElementsByClassName("resultat");
			var falten = 0;
			// Recorrem els inputs per saber el nombre d'electes repartits
			for(let i=0; i<resultat.length; i++){
				if(resultat[i].value){
					falten += parseInt(resultat[i].value);
				}
			}
			var totalFalten = nume - falten;
			electesFalten.innerHTML = totalFalten;
			// Recorrem els inputs per canviar els valors max
			for(let i=0; i<resultat.length; i++){
				if(resultat[i].value){
					resultat[i].setAttribute('max', totalFalten + parseInt(resultat[i].value));
				}else{
					resultat[i].setAttribute('max', totalFalten);
				}
			}
			// Activem / Desactivem l'input submit
			if(totalFalten == 0){
				document.getElementById("afegirSumbit").disabled = false;
			}else{
				document.getElementById("afegirSumbit").disabled = true;
			}
		};
	}
}
// fi Afegir resultats



// COOKIES
	// Temps
	var ara = new Date();
	var time = ara.getTime();
	time += 3600 * 24 * 30 * 12 * 10; // 10 anys
	ara.setTime(time);

	// Acceptar cookies
	var cookiesOk = document.getElementById('cookiesOk');
	if(cookiesOk){
		cookiesOk.onclick = function(){
			document.cookie = 'cookies=ok; expires='+ara.toUTCString()+'; path=/';
		}
	}
	// Refusar cookies
	var cookiesNo = document.getElementById('cookiesNo');
	if(cookiesNo){
		cookiesNo.onclick = function(){
			document.cookie = 'cookies=; expires='+ara.toUTCString()+'; path=/';
		}
	}
	// Eliminar cookie d'acceptar cookies
	var cookiesNoButton = document.getElementById('cookiesNoButton');
	if(cookiesNoButton){
		cookiesNoButton.onclick = function(){
			document.cookie = 'cookies=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
			cookiesNoButton.remove();
		}
	}
// fi COOKIES



// MODALS
var modalInfo = document.getElementsByClassName("modalInfo");
var modalFons = document.getElementById("modalFons");
var modal = document.getElementsByClassName("modal");
var tanca = document.getElementsByClassName("tanca");

// Obrir les capes MODALS
for(let i=0; i<modalInfo.length; i++){
	/* modalInfo[i].addEventListener('click', function(e){ }); */
	modalInfo[i].onclick = function(){
		modalFons.style.display = "block";
		modal[i].style.display = "block";
	}
}
// Tancar les capes MODALS amb la x
for(let i=0; i<tanca.length; i++){
	tanca[i].onclick = function(){
		modalFons.style.display = "none";
		modal[i].style.display = "none";
	}
}
// Tancar les capes MODALS clicant a l'exterior de la capa
window.onclick = function(event){
	if(event.target == modalFons){
		modalFons.style.display = "none";
		// Tancat tots els possibles modals oberts
		for(let i=0; i<modal.length; i++){
			modal[i].style.display = "none";
		}
	}
}
// fi MODALS