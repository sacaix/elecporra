<?php
include('funcions.php');

// URLs
$arrel = "/elecporra/";
$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$pag_host = $_SERVER['HTTP_HOST'];
$pag_uri00 = basename($_SERVER['REQUEST_URI']);
$pag_uri = parse_url($pag_uri00, PHP_URL_PATH);
$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2); // Seprar adreça dels get
$urlcompleta = $protocol . "$_SERVER[HTTP_HOST]$uri_parts[0]";
$urltotal = $protocol . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if(isset($_SERVER['HTTP_REFERER'])){
	$urlreferer = $_SERVER['HTTP_REFERER'];
}else{
	$urlreferer = "";
}
$dataActual = date("Y-m-d H:i:s");



// Cerca una PORRA
if(isset($_POST['cerca'])){
    $cerca = substr($_POST['cerca'],0,20); // Màxim 20 caracters
    $cerca = netejar($cerca);
    // Redirigim a la pàgina URL friendly
    header("Location: ".urlencode($cerca));
    exit();
}


// Nova PORRA
if(isset($_POST['novaPorra']) && !empty($_POST['novaPorra']) && isset($_POST['novaOculta']) && empty($_POST['novaOculta'])){
    $novaData = $dataActual;
    $novaNom = substr($_POST['novaPorra'],0,40); // Màxim 40 caracters
    $novaDesc = substr($_POST['novaDesc'],0,200); // Màxim 200 caracters
		// Eliminar els salts de línia
		$novaDesc = preg_replace( "/(\r|\n)/", " ", $novaDesc);
    $novaElectes = substr($_POST['novaElectes'],0,10); // Màxim 10 caracters
    if(empty($_POST['novaLimit'])){
        $novaLimit = "";
    }else{
        $novaLimit = date('Y-m-d H:i:s', strtotime($_POST['novaLimit']));
    }
    $novaCand = $_POST['novaCand']; // Array de candidatures
    $novesCand = '';
        $num = '0';
        foreach($novaCand as $nv){
            if(!empty($nv)){
                $nv = substr($nv,0,14); // Màxim 14 caracters
                if($num != 0){ $novesCand .= ","; }
                $novesCand .= netejar($nv);
                $num++;
            }
        }
    // Netegem l'string
    $novaNom = netejar($novaNom);
    $novaDesc = netejar($novaDesc);
    $novaElectes = netejar($novaElectes);
        if(empty($novaElectes)){ $novaElectes = 0; } // Si es queda en blanc posem 0
    $novesCand = netejar($novesCand);

    // Mentre el codi de la porra existeixi en creem un de nou
    do{
        $codiPorra = date("ymd").codiNumeric(10);
    }while(file_exists("docs/".$codiPorra.".json"));

    // Creem l'arxiu
    $arxiuw = fopen("docs/".$codiPorra.".json", "w") or die("Unable to open file!");
    $json = "{
    \"data\" : \"".$novaData."\",
    \"nom\" : \"".$novaNom."\",
    \"descripcio\" : \"".$novaDesc."\",
    \"electes\" : \"".$novaElectes."\",
    \"limit\" : \"".$novaLimit."\",
    \"candidatures\" : \"".$novesCand."\",
    \"final\" : \"\",
    \"porres\" : { }
}";
    fwrite($arxiuw, $json);
    fclose($arxiuw);

    // Desem la cookie amb les id de les porres
    afegirGaleta('porra', $codiPorra, '');

    // Indiquem que s'ha inserit una nova porra
    $pn = $codiPorra;
}




// Editar Porra
if(isset($_POST['editaPorra']) && !empty($_POST['editaPorra']) && isset($_POST['editaOculta']) && empty($_POST['editaOculta'])){
    $codiPorra = substr($_POST['codiPorra'],0,20); // Màxim 20 caracters
        $codiPorra = netejar($codiPorra);
        $codiPorra = str_replace(["'"," "], "", $codiPorra);
    $editaNom = substr($_POST['editaPorra'],0,40); // Màxim 40 caracters
    $editaDesc = substr($_POST['editaDesc'],0,200); // Màxim 200 caracters
    $editaElectes = substr($_POST['editaElectes'],0,10); // Màxim 10 caracters
    if(empty($_POST['editaLimit'])){
        $editaLimit = "";
    }else{
        $editaLimit = date('Y-m-d H:i:s', strtotime($_POST['editaLimit']));
    }
    $editaCand = $_POST['editaCand']; // Array de candidatures
    $editaCandS = '';
        $num = '0';
        foreach($editaCand as $ev){
            if(!empty($ev)){
                $ev = substr($ev,0,14); // Màxim 14 caracters
                if($num != 0){ $editaCandS .= ","; }
                $editaCandS .= netejar($ev);
                $num++;
            }
        }
    // Netegem l'string
    $editaNom = netejar($editaNom);
    $editaDesc = netejar($editaDesc);
    $editaElectes = netejar($editaElectes);
        if(empty($editaElectes)){ $editaElectes = 0; } // Si es queda en blanc posem 0
    $editaCandS = netejar($editaCandS);

    $arxiu = "docs/".$codiPorra.".json";
    if(file_exists($arxiu)){
        // Obrim l'arxiu JSON i el convertim en un array
        $jsonString = file_get_contents($arxiu);
        $jsonArray = json_decode($jsonString, true);
        // Comprovem que la porra la va crear l'usuari
        if(porraMeva($codiPorra)){
            // Introduim les noves dades
            $jsonArray['nom'] = $editaNom;
            $jsonArray['descripcio'] = $editaDesc;
            $jsonArray['electes'] = $editaElectes;
            $jsonArray['limit'] = $editaLimit;
            $jsonArray['candidatures'] = $editaCandS;
            // Convertim l'array a JSON i el desem
            $jsonStringNou = json_encode($jsonArray);
            file_put_contents($arxiu, $jsonStringNou);
            // Si el nom de la porra canvia, canviem les cookies
            if($jsonArray['nom'] != $editaNom){
                // Modificar la galeta per canviar el títol
                // PER DESENVOLUPAR
            }
        }
    }
}




// Nou resultat
if(isset($_POST['porrista']) && isset($_POST['nouOcult']) && empty($_POST['nouOcult'])){
    if(!empty($_POST['porrista'])){
        $nrPorra = substr($_POST['porraCodi'],0,20); // Màxim 20 caracters
        $nrPorraNom = substr($_POST['porraNom'],0,40); // Màxim 40 caracters
        $nrPorrista = substr($_POST['porrista'],0,28); // Màxim 28 caracters
            // Substituim les possibles dobles cometes
            $nrPorrista = netejar($nrPorrista);
        $nrElectes = $_POST['electes']; // Array de resultats
        $resultatFinal = false;

        $arxiu = "docs/".$nrPorra.".json";
        if(file_exists($arxiu)){
            // Obrim l'arxiu JSON i el convertim en un array
            $jsonString = file_get_contents($arxiu);
            $jsonArray = json_decode($jsonString, true);

            if(isset($_POST['final']) && porraMeva($nrPorra)){
                // Creem l'string de resultats finals
                $resultatFinal = '';
                $num = '0';
                    foreach($nrElectes as $ne){
                        // Elimiem els zeros a l'esquerra
                        while(substr($ne,0,1) == 0 && !empty($ne)){
                            $ne = substr($ne,1);
                        }
                        if(empty($ne)){ $ne = 0; }
                        if($num != 0){ $resultatFinal .= ","; }
                        $resultatFinal .= $ne;
                        $num++;
                    }

                // Afegim la diferència de cada porra amb el resultat final
                $numPorra = 0;
                foreach($jsonArray['porres'] as $cd){
                    $porraA = explode(',', $cd['porra']);
                    $num = 0;
                    $diferencia = 0;
                    foreach($porraA as $res){
                        // Calculem la diferència
                        $difParcial = 0;
                        if($res >= $nrElectes[$num]){
                            $difParcial = $res - $nrElectes[$num];
                        }else{
                            $difParcial = $nrElectes[$num] - $res;
                        }
                        $diferencia = $diferencia + $difParcial;
                        $num++;
                    }
                    $jsonArray['porres'][$numPorra]['diferencia'] = $diferencia;
                    $numPorra++;
                }
                // Ordenem els resultats per ordre d'aproximació
                usort($jsonArray['porres'], function($a, $b) {
                    if ($a['diferencia'] > $b['diferencia']) {
                        return 1;
                    } elseif ($a['diferencia'] < $b['diferencia']) {
                        return -1;
                    }
                    return 0;
                });

                // Desem el resultat final
                $jsonArray['final'] = $resultatFinal;
                // Convertim l'array a JSON i el desem
                $jsonStringNou = json_encode($jsonArray);
                file_put_contents($arxiu, $jsonStringNou);

                $resultatFinal = true;
            
            }else if(porraActiva($dataActual, $jsonArray['limit'])){
                // Creem un array amb els codis de resultats d'aquesta porra
                $arrayResPorra = [];
                foreach($jsonArray['porres'] as $cd){
                    $arrayResPorra[] = $cd['clau'];
                }
                // Creem l'string de resultats
                $nousResultats = '';
                    $num = '0';
                    foreach($nrElectes as $ne){
                        // Elimiem els zeros a l'esquerra
                        while(substr($ne,0,1) == 0 && !empty($ne)){
                            $ne = substr($ne,1);
                        }
                        if(empty($ne)){ $ne = 0; }
                        if($num != 0){ $nousResultats .= ","; }
                        $nousResultats .= $ne;
                        $num++;
                    }
                // Creem el codi de la porra
                do{
                    $codiNumRes = codiNumeric(8);
                }while(in_array($codiNumRes, $arrayResPorra));
                // Creem l'array dels nous resulats
                $arrayNou['nom'] = $nrPorrista;
                $arrayNou['clau'] = $codiNumRes;
                $arrayNou['porra'] = $nousResultats;
                // Desem l'array
                $jsonArray['porres'][] = $arrayNou;
                // Convertim l'array a JSON i el desem
                $jsonStringNou = json_encode($jsonArray);
                file_put_contents($arxiu, $jsonStringNou);
                // Desem la cookie amb les id de les porres
                afegirGaleta('resultat', $nrPorra, $codiNumRes);
            }
            $nouResultat = $nrPorra;
        }
    }
}


// Eliminar PORRA
if(isset($_POST['eliminar']) && !empty($_POST['eliminar']) && isset($_POST['eliminarOculta']) && empty($_POST['eliminarOculta'])){
    $codiPorra = $_POST['eliminar'];
    $nomPorra = $_POST['eliminarNom'];
    // Comprovem si la porra l'ha creat l'usuari mirant les galetes
    if(porraMeva($codiPorra)){
        // Eliminem la porra de la galeta de l'usuari
        eliminarGaletaPorra($codiPorra, true); // true = eliminar també la participació a aquella porra
        // Eliminem l'arxiu
        eliminarArxiu($codiPorra);
        $eliminada = $nomPorra;
    }
}

// Eliminar RESULTAT
if(isset($_POST['eliminarRes']) && !empty($_POST['eliminarRes']) && isset($_POST['codiPorra']) && !empty($_POST['codiPorra'])){
    $codiPorra = $_POST['codiPorra'];
    $codiRes = $_POST['eliminarRes'];

    $arxiu = "docs/".$codiPorra.".json";
    if(file_exists($arxiu)){
        // Obrim l'arxiu JSON i el convertim en un array
        $jsonString = file_get_contents($arxiu);
        $jsonArray = json_decode($jsonString, true);
        
        if(porraActiva($dataActual, $jsonArray['limit'])){
            // Comprovem si el resultat l'ha creat l'usuari mirant les galetes
            if(resultatMeu($codiPorra, $codiRes)){
                // Eliminem el resultat de l'arxiu JSON
                eliminarResultatArxiu($codiPorra, $codiRes);
                // Eliminem el resultat de la galeta de l'usuari
                eliminarGaletaResultat($codiPorra, $codiRes);
            }else{
                echo "El resultat no es meu";
            }
        }else{
            echo "S'ha passat la data límit per participar";
        }
    }else{
        echo "Error obrint l'arxiu";
    }
}


// Eliminar participació a una porra de les galetes
if(isset($_POST['eliminarParticipacio']) && !empty($_POST['eliminarParticipacio'])){
    $codiPorraPart = $_POST['eliminarParticipacio'];
    // Eliminem la galeta de la participació a aquesta porra
    eliminarGaletaParticipacio($codiPorraPart);
}

// Eliminar una porra de les galetes
if(isset($_POST['eliminarPorraGaleta']) && !empty($_POST['eliminarPorraGaleta'])){
    $codiPorra = $_POST['eliminarPorraGaleta'];
    // Eliminem la porra de la galeta de l'usuari
    eliminarGaletaPorra($codiPorra, false); // false = No eliminar la participació a aquella porra
}


// Eliminar Cookies
if(isset($_POST['eliminarCookies']) && empty($_POST['eliminarCookies'])){
    setcookie("porres", "", time() - 3600);
}
