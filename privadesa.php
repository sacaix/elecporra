<h1>Política de privadesa i seguretat</h1>
<h2><img class="colorCorall" src="imatges/candau01.svg" width="20" height="20"> Privadesa</h2>
<p>
    <em class='color'>elecPorra </em> emmagatzema només les dades que s'hagin introduït a través dels formularis de creació i participació a les porres.
</p>
<p>
    Tota la informació que es publica en aquest web és oberta i potencialment hi pot accedir qualsevol persona.
    No introduïu informació ni dades personals o comprometedores.
</p>
<p>
    Mai utilitzarem aquesta informació per a finalitats econòmiques. Mai es vendran o cediran a tercers cap de les dades obtingudes.
</p>

<h2><img class="colorCorall" src="imatges/galeta01.svg" width="20" height="20"> Cookies</h2>
<p>
    Aquesta pàgina web utilitza cookies (galetes), les quals són un arxiu d'informació que es desa al navegador de l'usuari,
    per tal de facilitar la navegació dels usuaris.
    En cap cas es desa aquesta informació a la pàgina web.
</p>
<p>
    La informació de les cookies està encriptada i s'utilitza per identificar quines porres s'han creat i a quines s'ha participat.
</p>
<p>
    <a class="enllaç" href="<?php echo $arrel; ?>cookies">
        <img class="colorCorall" src="imatges/galeta01.svg" width="20" height="20">
        Gestionar les galetes/cookies
    </a>
</p>