<?php
// Funció per crear un codi numèric aleatori
function codiNumeric($mida){
    $num = '';
    for($i=0; $i<$mida; $i++){
        $num = $num.rand(0,9);
    }
    return $num;
}


// Funció per netejar un string de caràcters potencialment maliciosos
function netejar($string){
    // Canviem les cometes dobles per simples
    $string = str_replace("\"","'",$string);
    // Eliminem caràcters especials
    $string = str_replace(['<','>','/','\\','*','+','-','--','|','=','$','{','}'], '', $string);
    // Eliminem espais en blanc a l'inici i al final
    $string = trim($string);
    return $string;
}


// Funció per saber si una porra està activa
// return true / false
function porraActiva($dataActual, $dataLimit){
    if(empty($dataLimit) OR (!empty($dataLimit) && $dataLimit > $dataActual)){
        return true;
    }else{
        return false;
    }
}


// ENCRIPTAR / DESENCRIPTAR
    // Mètode openssl_encrypt
    // https://www.php.net/openssl_encrypt
    // https://hotexamples.com/examples/-/-/openssl_encrypt/php-openssl_encrypt-function-examples.html

    function encriptar($dades, $clau){
        $encriptat00 = false;
        $encryption_method = "AES-256-CBC";
        $clau = hash('sha256', $clau);
        
        // Generate a random string, hash it and get the first 16 character of the hashed string which will be ised as the IV
        $str = "qwertyuiopasdfghjklzxcvbnm,./;'\\[]-=`!@#\$%^&*()_+{}|\":?><0123456789QWERTYUIOPASDFGHJKLZXCVBNM";
        $shuffled = str_shuffle($str);
        $iv = substr(hash('sha256', $shuffled), 0, 16);
        $encriptat00 = openssl_encrypt($dades, $encryption_method, $clau, 0, $iv);
        $encriptat00 = base64_encode($encriptat00);
        // Tidy up the string so that it survives the transport 100%
        $encriptat00 = $iv . $encriptat00;
            
        return $encriptat00;
    }

    function desencriptar($dades, $clau){
        $desencriptat00 = false;
        $encryption_method = "AES-256-CBC";
        $clau = hash('sha256', $clau);
        
        $iv = substr($dades, 0, 16);
        // Extract the IV from the encrypted string
        $dades = substr($dades, 16);
        // The rest of the encrypted string is the message
        $desencriptat00 = openssl_decrypt(base64_decode($dades), $encryption_method, $clau, 0, $iv);
                
        return $desencriptat00;
    }
// fi ENCRIPTAR / DESENCRIPTAR




// Gestió d'ARXIUS
    // Funció per eliminar un resultat de l'arxiu d'una porra
    function eliminarResultatArxiu($codiPorra, $codiRes){
        $arxiu = "docs/".$codiPorra.".json";

        if(file_exists($arxiu)){
            // Obrim l'arxiu JSON i el convertim en un array
            $jsonString = file_get_contents($arxiu);
            $jsonArray = json_decode($jsonString, true);

            // Eliminem
            $i=0;
            foreach($jsonArray['porres'] as $p) {
                // L'eliminem si és el resultat que volem eliminar
                if($codiRes == $p['clau']){
                    unset($jsonArray['porres'][$i]);
                }
                $i++;
            }
            // Renumerem les claus de l'array dels resultats
            $jsonArray['porres'] = array_values($jsonArray['porres']);

            // Convertim l'array a JSON i el desem
            $jsonStringNou = json_encode($jsonArray);
            file_put_contents($arxiu, $jsonStringNou);
        }
    }

    // Funció per eliminar un arxiu
    function eliminarArxiu($codiPorra){
        $arxiu = "docs/".$codiPorra.".json";
        if(file_exists($arxiu)) {
            unlink($arxiu);
        }
    }
// fi Gestió d'ARXIUS





// GALETES
/* Informació dins la galeta de les porres 
Exemple:
330085533451,230585135540--021501545875,124458+214578|122145765224,521547+241571
(porres - resultats)
Significat:
codi porra, codi porra -- codi porra, codi resultat + codi resultat | codi porra 2, codi resultat + codi resultat
*/

    // Funció per desar una galeta
    function desarGaleta($galeta){
        $galeta = encriptar($galeta, 'Gal0tësInflust1s');
        setcookie("porres", $galeta, time() + (3600 * 24 * 30)); // 1 hora * 1 dia * 1 mes
    }

    // Funció per manejar les galetes
    // $afegirTipus = porra / resultat
    function afegirGaleta($afegirTipus, $codiPorra, $codiResultat){
        // Llegir la cookie
        $porres = '';
        $resultats = '';
        if(isset($_COOKIE['porres'])){
            $galetaex = desencriptar($_COOKIE['porres'], 'Gal0tësInflust1s');
            $galetaex = explode('--',$galetaex);
            $porres = $galetaex[0];
            $resultats = $galetaex[1];
        }
        // Muntem la galeta
        if($afegirTipus == 'porra'){
            if(empty($porres)){
                $galetaNova = $codiPorra."--".$resultats;
            }else{
                $galetaNova = $porres.",".$codiPorra."--".$resultats;
            }
        }else if($afegirTipus == 'resultat'){
            if(empty($resultats)){
                $galetaNova = $porres."--".$codiPorra.",".$codiResultat;
            }else{
                // EN DESENVOLUPAMENT! ?
                // NO DESA ELS RESULTATS D'UNA SEGONA PORRA
                // Recorrem els resultats de la galeta per veure si ja hi ha resultats de la porra
                // a la que es vol guardar un altre resultat
                $resultatsex = explode('|', $resultats);
                $nousResultats = ''; $num = 0;
                $afegit = false;
                // Recorrem els resultats
                foreach($resultatsex as $r){
                    $rex = explode(',', $r);
                    if($num != 0){ $nousResultats .= "|"; }
                    // Si ja hi ha resultats en aquella porra, l'afegim
                    if($rex[0] == $codiPorra){
                        // Afegim el codi de la porra
                        $nousResultats .= $rex[0].",".$rex[1]."+".$codiResultat;
                        $afegit = true;
                    }else{
                        $nousResultats .= $rex[0].",".$rex[1];
                    }
                    $num++;
                }
                // Si no hi havia la porra, l'afegim amb els resultats
                if(!$afegit){
                    $nousResultats .= "|".$codiPorra.",".$codiResultat;
                }
                $galetaNova = $porres."--".$nousResultats;
            }
        }else{
            $galetaNova = '';
        }
        // Desem la galeta
        desarGaleta($galetaNova);
    }


    // Funció per llegir la galeta de les cookies està activa
    // return true / false
    function llegirGaletaCookies(){
        if(isset($_COOKIE['cookies'])){
            return $_COOKIE['cookies'];
        }else{
            return "";
        }
    }
    

    // Funció per llegir les galetes
    // $tipus = porres / resultats / tot
    // return String de porres o resultats
    function llegirGaleta($tipus){
        $tot = '';
        $porres = '';
        $resultats = '';
        // Llegir la cookie
        if(isset($_COOKIE['porres']) && !empty($_COOKIE['porres'])){
            $tot = desencriptar($_COOKIE['porres'], 'Gal0tësInflust1s');
            $galetaex = explode('--',$tot);
            if(!empty($galetaex[0])){
                $porres = $galetaex[0];
            }
            if(!empty($galetaex[1])){
                $resultats = $galetaex[1];
            }
        }
        // Retornem l'string
        if($tipus == 'porres'){
            return $porres;
        }else if($tipus == 'resultats'){
            return $resultats;
        }else{
            return $tot;
        }
    }

    // Funció per llegir els resultats de les galetes
    // return Array del codi de resultats d'una porra
    function galetaResultatsPorra($codiPorra){
        $stringResultats = llegirGaleta('resultats');
        $res = explode('|',$stringResultats);
        $arrayRes = [];
        foreach($res as $r){
            $r = explode(',',$r);
            if($r[0] == $codiPorra){
                if(!empty($r[1])){
                    $rp = explode('+',$r[1]);
                    foreach($rp as $cr){
                        $arrayRes[] = $cr;
                    }
                }
            }
        }
        return $arrayRes;
    }

    // Funció per determinar si una porra és de l'usuari
    // return true / false
    function porraMeva($p){
        $stringPorres = llegirGaleta('porres');
        $res = explode(',',$stringPorres);
        foreach($res as $r){
            if($r == $p){
                return true;
            }
        }
        return false;
    }

    // Funció per eliminar una porra de la galeta
    // return true / false
    function eliminarGaletaPorra($p, $eliminarParticipacio){
        $stringGaleta = llegirGaleta('tot');
        $galetaex = explode('--',$stringGaleta);
        $porres = $galetaex[0];
        $resultats = $galetaex[1];

        $porresNoves = '';
        if(!empty($porres)){
            $num = 0;
            $res = explode(',',$porres);
            foreach($res as $r){
                if($r != $p){
                    if($num != 0){ $porresNoves .= ','; } // Afegim la coma si no és la primera
                    $porresNoves .= $r;
                    $num++;
                }
            }
        }
        if($eliminarParticipacio){
            // Eliminar Resultats de la porra que s'elimina, si és que n'hi ha
            $resultatsNous = '';
            if(!empty($resultats)){
                $num = 0;
                $res = explode('|',$resultats);
                foreach($res as $r){
                    $rex = explode(',',$r);
                    if($rex[0] != $p){
                        if($num != 0){ $resultatsNous .= '|'; } // Afegim el separador si no és la primera
                        $resultatsNous .= $r;
                        $num++;
                    }
                }
            }
        }else{
            // Mantenim els resultats iguals
            $resultatsNous = $resultats;
        }
        $galetaNova = $porresNoves."--".$resultatsNous;
        // Desem la galeta
        desarGaleta($galetaNova);
    }

    // Funció per determinar si el resultat d'una porra és de l'usuari
    // return true / false
    function resultatMeu($porra, $resultat){
        $stringRes = llegirGaleta('resultats');
        $res = explode('|',$stringRes);
        foreach($res as $r){
            $rex = explode(',',$r);
            if($rex[0] == $porra){
                $cex = explode('+',$rex[1]);
                foreach($cex as $c){ // Recorrem els resultats
                    if($c == $resultat){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // Funció per eliminar el resultat d'porra de la galeta
    function eliminarGaletaResultat($codiPorra, $codiRes){
        $stringGaleta = llegirGaleta('tot');
        $galetaex = explode('--',$stringGaleta);
        $porres = $galetaex[0];
        $resultats = $galetaex[1];

        // Desem l'string de porres
        if(!empty($porres)){
            $porresNoves = $porres;
        }else{
            $porresNoves = '';
        }
        // Eliminar el resultat de la porra
        $resultatsNous = '';
        if(!empty($resultats)){
            $num = 0;
            $res = explode('|',$resultats);
            foreach($res as $r){ // Recorrem les porres amb resultats
                if($num != 0){ $resultatsNous .= '|'; } // Afegim el separador si no és la primera
                $rex = explode(',',$r);
                if($rex[0] == $codiPorra){
                    $rec = explode('+',$rex[1]);
                    // Si hi ha més d'un resultat en aquesta porra / Si no, no desem res
                    if(sizeof($rec) > 1){
                        $num2 = 0;
                        // Afegim el codi i el nom
                        $resultatsNous .= $rex[0].",";
                        foreach($rec as $c){ // Recorrem els resultats d'una porra
                            if($c != $codiRes){
                                if($num2 != 0){ $resultatsNous .= '+'; } // Afegim el separador si no és la primera
                                // Afegim el codi del resultat
                                $resultatsNous .= $c;
                                $num2++;
                            }
                        }
                        $num++;
                    }
                }else{
                    $resultatsNous .= $r;
                    $num++;
                }
            }
        }
        $galetaNova = $porresNoves."--".$resultatsNous;
        // Desem la galeta
        desarGaleta($galetaNova);
    }



    // Funció per eliminar les participacions a una porra de les galetes
    function eliminarGaletaParticipacio($codiPorra){
        $stringGaleta = llegirGaleta('tot');
        $galetaex = explode('--',$stringGaleta);
        $porres = $galetaex[0];
        $resultats = $galetaex[1];

        // Desem l'string de porres
        if(!empty($porres)){
            $porresNoves = $porres;
        }else{
            $porresNoves = '';
        }
        // Eliminar el resultat de la porra
        $resultatsNous = '';
        if(!empty($resultats)){
            $num = 0;
            $res = explode('|',$resultats);
            foreach($res as $r){ // Recorrem les porres amb resultats
                $rex = explode(',',$r);
                if($rex[0] != $codiPorra){
                    if($num != 0){ $resultatsNous .= '|'; } // Afegim el separador si no és la primera
                    $resultatsNous .= $r;
                    $num++;
                }
            }
        }
        $galetaNova = $porresNoves."--".$resultatsNous;
        // Desem la galeta
        desarGaleta($galetaNova);
    }

// fi GALETES