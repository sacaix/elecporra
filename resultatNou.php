<?php
// Nou resultat / Pantalla intermitja
if($resultatFinal){
    ?>
    <div class=flex>
        <div class='flex1'>
            <h1>Resultat final</h1>
            <h2>Has introduït el resultat final de la porra</h2>
            <p>
                Moltes gràcies per utilitzar <strong class="color">elecPorra</strong>!
            </p>
            <p class='alineaCentre'>
                <form method="post" action="<?php echo $arrel.$p; ?>">
                    <button type="submit" class="enviar">TORNA A LA PORRA</button>
                </form>
            </p>
        </div>
    </div>
    <?php
}else{
    ?>
    <div class=flex>
        <div class='flex1'>
            <h1>Gràcies per participar!</h1>
            <h2>S'ha introduït la teva resposta a la porra</h2>
            <p>
                Podràs eliminar el resultat des d'aquest mateix dispositiu, sempre que la porra segueixi activa.
            </p>
            <p class='alineaCentre'>
                <form method="post" action="<?php echo $arrel.$p; ?>">
                    <button type="submit" class="enviar">TORNA A LA PORRA</button>
                </form>
            </p>
        </div>
    </div>
    <?php
}
